from django.urls import path

from . import views

app_name = 'app'
urlpatterns = [
    path('', views.index, name='index'),
    path('<int:image_id>/', views.detail, name='detail'),
    path('<int:image_id>/comment', views.comment, name='comment'),
    path('approve/<int:comment_id>', views.approve_comment, name='approve_comment'),
    path('delete/<int:comment_id>', views.delete_comment, name='delete_comment'),
    path('like_comment/<int:comment_id>', views.like_comment, name='like_comment'),
    path('new', views.create_image , name='create_image'),
    path('<int:image_id>/edit', views.update_image, name="update_image"),
    path('<int:image_id>/delete', views.delete_image, name="delete_image"),
    path('<int:image_id>/upvote', views.upvote, name='upvote'),
    path('<int:image_id>/downvote', views.downvote, name='downvote'),
    path('accounts/<int:user_id>', views.account, name='account'),
]
