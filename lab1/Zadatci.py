def prvi():
    ime=input("Kako se zoves ")
    godine=input("Koliko si star ")
    g=2022-int(godine)+100
    print("Vase ime je "+ ime +" imate "+ godine +" godina" + "U " + str(g) + " imat ces 100 godina" )

def drugi():
    broj=int(input("Unesi broj koji zelis provjeriti "))
    if broj%2==0:
        print("Paran je ")
    else:
        print("Neparan je ")

def treci():
    a = [1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89]
    for x in a:
        if x< 5: print(x)

def cetvrti():
    i=2
    broj=int(input("Unesi broj koji zelis provjeriti "))
    
    while (i<=broj/2):
        if broj%i==0:
            print(i ," ")
        i=i+1

def peti():
    a = [1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89]
    b = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13]
    zajednicki=[]

    for l1 in a:
        for l2 in b:
            if(l1==l2):
                zajednicki.append(l1)
    
    print("Zajednicki elementi")
    for number in zajednicki:
        print(number)

def sesti():
    pal=input("Provjeri dali je palindrom ")
    pal2 = pal[::-1]
    if pal2 == pal :
        print ("palindrom je")
    else:
        print ("nije palindrom")

def sedmi():
    a = [1, 4, 9, 16, 25, 36, 49, 64, 81, 100]
    nl = []
    for i in range (1,len(a)):
        if a[i]%2==0:
            nl.append(a[i])
    print(nl)



def comma_sep_to_int(values):
    numbers = []
    for number in values.split(","):
        if(number != ""):
            numbers.append(int(number))
    return numbers

def count_evens(numbers):
    even_count = 0
    for x in numbers:
        if x % 2 == 0:
            even_count += 1
    return even_count

def occurences(numbers, number):
    occurencesInList = 0
    for x in numbers:
        if number == x:
            occurencesInList += 1
    return occurencesInList

def centered_average(numbers):
    l_min = min(numbers)
    l_max = max(numbers)
    filteredList = []
    for i in numbers:
        if (i != l_min and i != l_max):
            filteredList.append(i)
        else:
            if occurences(numbers,i)>1 and i not in filteredList:
                filteredList.append(i)
    return filteredList

def char_types_count(data):
    upperCount = sum(1 for c in data if c.isupper())
    lowerCount = sum(1 for c in data if c.islower())
    numbersCount = sum(1 for c in data if c.isnumeric())
    return(upperCount, lowerCount, numbersCount)

def exercise1():
    entered_string = input("Input comma separated numbers: ")
    numbers = comma_sep_to_int(entered_string)
    print("Number of evens: " + str(count_evens(numbers)))

def exercise2():
    numbers = comma_sep_to_int(input("Input coma separated: "))
    List=[]
    List=centered_average(numbers)
    print(len(List))

def exercise3():
    found = False
    numbers = comma_sep_to_int(input("Input comma separated: "))
    for i in range(0, len(numbers) - 1):
        if numbers[i] == 2 and numbers[i + 1] == 2:
            found = True
    print(found)

def exercise4():
    data = input("Input string: ")
    print(char_types_count(data))

def main():
    exercise4()


if __name__ == "__main__":
    main()