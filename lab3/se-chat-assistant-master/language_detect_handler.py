from query_handler_base import QueryHandlerBase
import random
import requests
import json


class LanguageHandler(QueryHandlerBase):
    def can_process_query(self, query):
        if "language" in query:
            return True
        return False

    def process(self, query):
        detect = query


        try:
            result = self.call_api(detect)
            conf = result["confidence"]
            lang = result["language"]
            reli = result["language"]
            self.ui.say(f"Detected language is: {lang}")
            self.ui.say(f"Confidence is: {conf}")
            self.ui.say(f"Realibility: {reli}")
        except: 
            self.ui.say("Oh no! There was an error trying to contact Google translate api.")
            self.ui.say("Try something else!")




    def call_api(self, detect):
        url = "https://google-translate1.p.rapidapi.com/language/translate/v2/detect"

        payload = "q="+detect
        headers = {
            "content-type": "application/x-www-form-urlencoded",
            "Accept-Encoding": "application/gzip",
            "X-RapidAPI-Key": "1357f9d6b0msh1044bb48a990d93p1b339fjsn418fd9c6f095",
            "X-RapidAPI-Host": "google-translate1.p.rapidapi.com"
        }

        response = requests.request("POST", url, data=payload, headers=headers)
        print(response)
        return json.loads(response.text)
